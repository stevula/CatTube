# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

authors = []
3.times do
  authors << User.create(
    email: Faker::Internet.email,
    password: "password"
  )
end

20.times do
  video = Video.create(
    title: Faker::Book.title,
    url: Faker::Internet.url,
    upvotes: rand(1000),
    downvotes: rand(100),
    author_id: authors.sample.id
  )

  10.times do
    video.comments.create(
      body: Faker::Hacker.say_something_smart,
      author_id: authors.sample.id
    )
  end
end
