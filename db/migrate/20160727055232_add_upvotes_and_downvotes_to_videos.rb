class AddUpvotesAndDownvotesToVideos < ActiveRecord::Migration[5.0]
  def change
    add_column :videos, :upvotes, :integer, default: 0
    add_column :videos, :downvotes, :integer, default: 0
  end
end
