class AddForeignKeyVideoIdToComments < ActiveRecord::Migration[5.0]
  def change
    add_reference :comments, :video, index: true
  end
end
