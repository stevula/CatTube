class AddForeignKeyAuthorIdToCommentsAndVideos < ActiveRecord::Migration[5.0]
  def change
    add_reference :comments, :author, index: true
    add_reference :videos, :author, index: true
  end
end
