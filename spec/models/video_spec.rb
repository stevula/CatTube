require 'rails_helper'

RSpec.describe Video, type: :model do
  let(:video_instance) {build(:video)}
  let(:video) {create(:video)}

  it 'requires a title' do
    video = video_instance
    video.title = nil
    expect(video).to be_invalid
    video.title = ""
    expect(video).to be_invalid
  end

  it 'requires a URL' do
    video.url = nil
    expect(video).to be_invalid
    video.url = ""
    expect(video).to be_invalid
  end

  describe '#upvote' do
    it 'increases upvotes by 1' do
      vid = video
      expect{vid.upvote}.to change{vid.upvotes}.by(1)
    end
  end

  describe '#downvote' do
    it 'increases downvotes by 1' do
      vid = video
      expect{vid.downvote}.to change{vid.downvotes}.by(1)
    end
  end

  describe '#net_votes' do
    it 'returns the net votes of upvotes and downvotes' do
      expect(video.net_votes).to be 8
    end
  end
end
