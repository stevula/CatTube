require 'rails_helper'

RSpec.describe Vote, type: :model do
  let(:vote_instance) {build(:vote)}
  let(:vote) {create(:vote)}

  it 'requires a value of 1 or -1' do
    vote = vote_instance
    vote.value = nil
    expect(vote).to be_invalid
    vote.value = 0
    expect(vote).to be_invalid
    vote.value = -2
    expect(vote).to be_invalid
    vote.value = 2
    expect(vote).to be_invalid
    vote.value = -1
    expect(vote).to be_valid
    vote.value = 1
    expect(vote).to be_valid
  end

  # TODO: feature test - user's vote should only count once but still be recorded
end
