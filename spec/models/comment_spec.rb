require 'rails_helper'

RSpec.describe Comment, type: :model do
  let(:comment) {create(:comment)}
  let(:comment_instance) {build(:comment)}

  it 'requires a body' do
    comment = comment_instance
    comment.body = nil
    expect(comment).to be_invalid
    comment.body = ""
    expect(comment).to be_invalid
  end
end
