FactoryGirl.define do
  factory :video do
    title "Test Video"
    url "test.video.com"
    upvotes 10
    downvotes 2
  end
end
