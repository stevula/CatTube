class Comment < ApplicationRecord
  belongs_to :video
  belongs_to :author, class_name: User

  validates :body, length: {minimum: 1}, presence: true
end
