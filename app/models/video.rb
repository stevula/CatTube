class Video < ApplicationRecord
  has_many :comments
  has_many :votes
  belongs_to :author, class_name: User

  validates :title, length: {minimum: 1}, presence: true
  validates :url, length: {minimum: 1}, presence: true
  validates :upvotes, numericality: true, presence: true
  validates :downvotes, numericality: true, presence: true

  def upvote
    self.increment!(:upvotes, 1)
  end

  def downvote
    self.increment!(:downvotes, 1)
  end

  def net_votes
    self.upvotes - self.downvotes
  end
end
