class Vote < ApplicationRecord
  belongs_to :voter, class_name: User
  belongs_to :video

  validates :value, inclusion: {in: [-1, 1]}
end
