class VotesController < ApplicationController
  before_action :authenticate_user!

  def create
    video = Video.find(params[:video_id])
    vote = video.votes.build(value: params[:value], voter_id: current_user.id)
    if vote.save!
      vote_value = params[:value].to_i
      if vote_value > 0
        video.upvote
      elsif vote_value < 0
        video.downvote
      end
    end
    redirect_to video_path(video)
  end
end
